#import internal packages
from selenium import webdriver
import time
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import login_credientials
import urls_test
import search_product_and_filter
import xlsxwriter
from selenium.webdriver.common.by import By

driver = webdriver.Firefox()

driver.implicitly_wait(0.5)
#ActionChains are a way to automate low level interactions such as mouse movements, mouse button actions, key press, and context menu interactions. 
action = ActionChains(driver)

test_case_pass = []
test_case_fail = []

# Test 1 
try:
    driver.get(urls_test.amazon_url)
    if urls_test.amazon_url in driver.current_url:
        test_case_pass.append("Test case 1 Pass: Go to amazon website.")
    else:
        test_case_fail.append("Test case 1 : Fail.")
except NoSuchElementException:
    test_case_fail.append("Test case 1 : Element not found .Test case fail.")

# Test 2
try:
    hovour_on_account = driver.find_element(by=By.XPATH, value='//*[@id="nav-link-accountList"]')
    action.move_to_element(hovour_on_account).perform()
    test_case_pass.append("Test Case 2 Pass : Hover on Account and sign in.")
    time.sleep(2)
except NoSuchElementException:
    test_case_fail.append("Test case 2 : Element not found .Test ase fail.")

#Test 3
try:
    sign_in_button = driver.find_element(by=By.XPATH, value="//a[@data-nav-role='signin']")
    action.move_to_element(sign_in_button).click().perform()
    test_case_pass.append("Test Case 3 Pass : Click on sign in button .")
    time.sleep(2)
except NoSuchElementException:
    test_case_fail.append("Test Case 3 : Element not found. Test case fail.")



#Test 4
if urls_test.signin_url in driver.current_url:
    try:
        #Enter email address or mobile number.
        email_or_phone = driver.find_element(by=By.XPATH, value='//*[@id="ap_email"]').send_keys(login_credientials.USERNAME)
        time.sleep(2)
        #Click Continue button.
        username_continue = driver.find_element(by=By.XPATH, value='//*[@id="continue"]').click()
        time.sleep(2)
        test_case_pass.append("Test Case 4 Pass : Enter username and click on continue.")
    except NoSuchElementException:
        test_case_fail.append("Test Case 4 : Element not found. Test case fail.")
else:
    test_case_fail.append("Test Case 4 : Fail.")

#Test 5

if urls_test.password_url in driver.current_url:
    try:
        enter_password = driver.find_element(by=By.XPATH, value='//*[@id="ap_password"]').send_keys(login_credientials.PASSWORD)
        time.sleep(2)
    # Sign in button click.
        sign_in = driver.find_element(by=By.XPATH, value='//*[@id="signInSubmit"]').click()
        time.sleep(2)
        test_case_pass.append("Test Case 5 Pass: Enter Password and click in sign in button.")

    except NoSuchElementException:
        test_case_fail.append('Test Case 5 : Element not found. Test case fail')
else:
    test_case_fail.append("Test Case 5 : Failed")

#Test 6
try:
    searchbar = driver.find_element(by=By.XPATH, value='//*[@id="twotabsearchtextbox"]')
    searchbar.send_keys(search_product_and_filter.SEARCH_PRODUCT)
    searchbar.send_keys(Keys.ENTER)
    time.sleep(2)
    test_case_pass.append("Test case 6 Pass : Search Product.")
except NoSuchElementException:
    test_case_fail.append("Test case 6 : Element not found. Test case fail.")

#Test 7
try:
    min_price = driver.find_element(by=By.XPATH, value='//*[@id="low-price"]').send_keys(search_product_and_filter.MIN_PRICE)
    time.sleep(2)
    max_price = driver.find_element(by=By.XPATH, value='//*[@id="high-price"]').send_keys(search_product_and_filter.MAX_PRICE)
    time.sleep(2)
    filter_go = driver.find_element(by=By.XPATH, value="//input[@type='submit' and @aria-labelledby='a-autoid-1-announce']").click()
    time.sleep(2)
    test_case_pass.append('Test case 7 Pass : Enter Min and Max price and click on go button.')
except NoSuchElementException:
    test_case_fail.append("Test case 7 : Element not found. Test case fail.")


workbook = xlsxwriter.Workbook('amazon_test_case.xlsx')
worksheet = workbook.add_worksheet('pass_test_case')

row = 1
column = 0

for item in test_case_pass :
    worksheet.write('A1', 'Pass Test Cases') 
    worksheet.write(row, column, item)
    row += 1

if len(test_case_fail) > 0:
    worksheet1 = workbook.add_worksheet('fail_test_case')

    for item in test_case_fail:
        worksheet1.write('A1', 'Fail Test Cases')
        worksheet1.write(row, column, item)
        row +=1

workbook.close()

driver.close()