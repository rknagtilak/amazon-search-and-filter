
# Selenium Amazon Testing

Selenium Amazon search products and filter products.



## Installation

Step by Step Installation this project.

```bash
  python -m venv <venv_name>

  source <venv_name>/bin/activate

  git clone https://gitlab.com/rknagtilak/amazon-search-and-filter.git

  pip install -r requirements.txt

```
Finally done our setup.

Please enter your amazon username and password in login credntials (login_credientials.py)

example :-
```
USERNAME = 'Please enter your email or mobile.'
PASSWORD = 'Please enter your password.'

```

Now You can run the program.
```
  python amazon_auto_test.py
  
```

Wait for movement our testing is in processing now please check auto genrated excel file.
In this file two sheet genrated Pass Test Case & Fail Test Case
```
  amazon_test_case.xlsx 
```
Please check excel for our amazon automation test case pass or fail.
Testing complete now ```Thank You.```
    